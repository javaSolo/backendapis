<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class CMSSession extends Model
{
   

    protected $table = "tbl_cms_session";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'app_token',  'user', 'status'
    ];

    public $timestamps = true;

    public const CREATED_AT ="login_time";
    public const UPDATED_AT  ="login_time";

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'app_token',
    ];
}
