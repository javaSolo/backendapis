<?php
    namespace App\Http\Controllers;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Crypt ;

    use Illuminate\Http\Request;
    use App\Users;
  
    use App\NEEV\dto\cms\request\NewUserDTO;
    use App\NEEV\utils\ValidateJSON;

    use PHPUnit\Runner\Exception;
    use App\User;
    use App\CMSUser;
    use Illuminate\Support\Facades\Response;
    use App\NEEV\dto\cms\response\ResponseDTO;

    use Illuminate\Hashing\BcryptHasher;
    //use Illuminate\Support\Facades\Crypt;

    /*
    *
    * Controller class with logic of handle CMS user data
    * Methods for adding new cms adding, updating,  deleting and get list exists user 
    *
    * @author Sivakumar Murugan
    *
    */

    class CMSUserController extends Controller{
        public function __construct(){ }
    
        /*
        * Method to add new user'
        * Called on route /new_user
        * @param $request - Http Request object to get all current request details
        * prints JSON response
        * @return none
        */

        public function newUSer(Request $request){

            // Get JSON input from http request in array format
            $input_JSON = $request->json()->all();

            // create instance of DTO class for validation
            $new_user_obj = new NewUserDTO();

            // if not valid exception is thrown
            try{
                new ValidateJSON($input_JSON, $new_user_obj);
            }catch(Exception  $e){
                echo $e->getMessage();
            }

            // create new user, return id on success
            $id=  $this->createObject($input_JSON);

            //if success print json response
            if($id>0){
                $reponse = new ResponseDTO("200", "User created");
                // get_object_vars converts PHP class into array list of accessible object's variable
                echo json_encode(get_object_vars($reponse));
            }

        }

        private function createObject($input_JSON){
            $user = new CMSUser();
            $user->first_name = $input_JSON["firstName"];
            $user->last_name = $input_JSON["lastName"];
            $user->email = $input_JSON["email"];
            $user->mobile = $input_JSON["mobile"];
            $password_text = $input_JSON["password"];
            $user->password = Hash::make($password_text);  
            $user->save();
            return $user->id;
        }

       

    }




?>