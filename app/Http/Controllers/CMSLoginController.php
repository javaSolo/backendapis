<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;

    use App\CMSUser;
    use App\NEEV\dto\cms\request\LoginDTO;
    use App\NEEV\utils\ValidateJSON;
    use App\NEEV\dto\cms\response\LoginResponse;

    use Symfony\Component\HttpFoundation\Response;
    use App\CMSSession;
    use Illuminate\Hashing\BcryptHasher;

    class CMSLoginController{

        public function authenicate(Request $request){
            
            // get arrays of input json value
            $input_JSON_array = $request->json()->all();

            // create object of expected input JSON structure
            $login_dto = new LoginDTO();

            try{
                new ValidateJSON($input_JSON_array, $login_dto);
            }catch(Exception $e){
                echo $e.getMessage();
            }
            
            $username = $input_JSON_array["username"];
            $plain_password= $input_JSON_array["password"];
            
            //$password_hash = Hash::make($plain_password);

            /*$hashedPassword = (new BcryptHasher)->make($plain_password);
            var_dump($hashedPassword);*/

            $user =  $this->checkUser($username, $plain_password);

            if($user["id"]){

                $response = new LoginResponse("200", "OK");

                $app_token= $this->createSession($user["id"]);

                $response->success($app_token,$user);
                $response_json = json_encode(get_object_vars($response));
                return new Response($response_json);
            }else{
                $response = new LoginResponse("402", "Invalid credentials.");
                $response->failed(false);
                $response_json = json_encode(get_object_vars($response));
                return new Response($response_json);
            }

        }


        /*
        *
        * Method to check whether provided username password has any matching recorrds 
        * @param $username : String of username
        * @param $password : String of password text
        * @return instance of user record matched
        *
        */

        public static final function checkUser($username, $password){
            
            $user_record = CMSUser::where([
                'email' => $username,
            ])->orWhere([
                'mobile' => $username,
            ])->where('status', 1)->first();
            
            $hashed_password = $user_record["password"];

            if(Hash::check($password, $hashed_password)){
               
                return $user_record;
            }
            
            return false;
                
        }

        public function createSession($user_id){

            $cms_session =  new CMSSession();
            $cms_session->user = $user_id;
            $cms_session->app_token = Hash::make($user_id);
            $cms_session->status = 1;
            $cms_session->save();
            return $this->encodeToken($cms_session->id);

        }

        public static function encodeToken($id){
            return base64_encode($id);
        }
    }
?>