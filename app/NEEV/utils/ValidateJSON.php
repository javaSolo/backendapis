<?php


namespace App\NEEV\utils;
use Exception;

class ValidateJSON{
    
     // validate if input json is as per the DTO class specified
     public function __construct($input_JSON, $object){

        $object_array = get_object_vars($object);

        $object_array_attr = array_keys($object_array);
        $input_JSON_attr = array_keys($input_JSON);

        if(sizeOf(array_diff($object_array_attr, $input_JSON_attr))>0){
            throw new Exception("Invalid input JSON");
        }
    }
}

?>