<?php

    namespace App\NEEV\dto\cms\response;
    use Illuminate\Http\Response;

    class LoginResponse extends ResponseDTO{

        public $authentication;
        public $appToken;
        public $userData;

        public function failed($authentication){
            $this->authentication = $authentication;
        }

        public function success($appToken, $userData){
            $this->appToken = $appToken;
            $this->authentication = true;
            $this->userData = [
                                "id" => $userData["id"], 
                                "first_name" => $userData["first_name"], 
                                "last_name" => $userData["last_name"],
                                "email" => $userData["email"],
                                "mobile" => $userData["mobile"]
                              ];
        }

    }

?>