<?php

    namespace App\NEEV\dto\cms\response;

    class ResponseDTO
    {

        public $status = "";
        public $message = "";

        public function __construct($status, $message){
            $this->status = $status;
            $this->message = $message;
        }
    }

?>