<?php

    namespace App\NEEV\dto\cms\request;

    class NewUserDTO{
        public $firstName;
        public $lastName;
        public $email;
        public $password;
        public $mobile;
    }

?>